#ifndef BALL_H
#define BALL_H

#include <SDL.h>
#include "GameObject.h"
#include "Globals.h"
#include <cmath>

class Ball : public GameObject {
public:
    Ball() : GameObject(), mRect({ mX, mY, mW, mH }), mColor({ 255, 255, 255, 255 }), mExternalAnchor(NULL) {
        Reset();
    }

    void Update(const double dt);
    void Draw(SDL_Renderer* renderer, const double interpolation) const;
    void Reset();
    void FlipXVelocity() { mVelX *= -1; }
    void FlipYVelocity() { mVelY *= -1; }
    void Bounce(const float angle);

    void SetAnchor(Anchor* a) { mExternalAnchor = a; }
    void ReleaseAnchor() {
        mExternalAnchor = NULL;
    }

    Anchor* GetExternalAnchor() const { return mExternalAnchor; }
    bool IsAnchored() const { return mExternalAnchor != NULL; }

    bool IsMoving() const { return mVelX != 0.0f && mVelY != 0.0f; }

    void SetSpeed(float value) { mSpeed = value; }

private:
    float mSpeed;
    float mVelX;
    float mVelY;

    SDL_Color mColor;
    SDL_Rect mRect;
    Anchor* mExternalAnchor;
};
#endif