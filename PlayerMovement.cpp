#include "PlayerMovement.h"

void PlayerMovement::Update(const double dt, const Ball* const ball = NULL) {
    const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);

    if (currentKeyStates[SDL_SCANCODE_UP])
        mParent->Move(0, -speed * dt);
    if (currentKeyStates[SDL_SCANCODE_DOWN])
        mParent->Move(0, speed * dt);

    if (mParent->GetY() < 0)
        mParent->SetY(0);
    else if (mParent->GetY() + mParent->GetHeight() > SCREEN_HEIGHT)
        mParent->SetY(SCREEN_HEIGHT - mParent->GetHeight());
}