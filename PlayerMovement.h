#ifndef PLAYERMOVEMENT_H
#define PLAYERMOVEMENT_H

#include <SDL.h>
#include "MovementComponent.h"

class PlayerMovement : public MovementComponent {
public:
    PlayerMovement(GameObject* parent) : MovementComponent(parent) { };

    void Update(const double, const Ball* const) override;
};
#endif