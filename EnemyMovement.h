#ifndef ENEMYMOVEMENT_H
#define ENEMYMOVEMENT_H

#include <SDL.h>
#include "MovementComponent.h"
#include "GameObject.h"
#include "Ball.h"
#include "Random.h"

class EnemyMovement : public MovementComponent {
public:
    EnemyMovement(GameObject* parent) : MovementComponent(parent) { offset = 0; nextRandTick = 0; }

    void Update(const double, const Ball* const) override;

private:
    Random rand;
    int offset;
    Uint32 nextRandTick; // for random offset to lerp to
};

#endif