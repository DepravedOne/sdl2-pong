#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <iostream>
#include "Globals.h"
#include "GameManager.h"

#undef main

bool Init();
void Close();

SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;

GameManager* gGameManager = NULL;

int main(int argc, int argv[]) {
    // Initialize SDL
    if (!Init()) {
        Close();  
        return 1;
    }
    std::cout << "SDL initialized." << std::endl;

    // Create game manager
    gGameManager = new GameManager(gRenderer);
    if (!gGameManager->Init()) {
        Close();
        return 2;
    }
    std::cout << "Game manager initialized." << std::endl;

    gGameManager->Run();

    Close();
    return 0;
}

bool Init() {
    // Initialization flag
    bool succes = true;

    // Initialize SDL
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        succes = false;
    }
    else {
        // Create window
        gWindow = SDL_CreateWindow("SDL2 Pong", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (gWindow == NULL) {
            std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
            succes = false;
        }
        else {
            // Create renderer for window
            gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (gRenderer == NULL) {
                std::cout << "Renderer could not be created! SDL_Error: " << SDL_GetError() << std::endl;
                succes = false;
            }
            else {
                // Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if (!(IMG_Init(imgFlags) & imgFlags)) {
                    std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
                    succes = false;
                }

                // Initialize SDL_ttf
                if (TTF_Init() == -1) {
                    std::cout << "SDL_ttf could not initialize! SDL_ttf Error: " << TTF_GetError() << std::endl;
                    succes = false;
                }

            }
        }
    }
    return succes;
}

void Close() {
    // Destroy game manager
    delete gGameManager;
    gGameManager = NULL;

    // Destroy window
    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;
    gRenderer = NULL;

    // Quit SDL subsystems
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}