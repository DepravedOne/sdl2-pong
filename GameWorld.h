#ifndef GAMEWORLD_H
#define GAMEWORLD_H

#include <SDL.h>
#include <queue>

class GameWorld {
public:
    GameWorld() : mLoaded(false), mQuit(false) { 
        std::queue<SDL_Event> empty; 
        std::swap(eventQueue, empty);
    }
    virtual ~GameWorld() { }

    virtual bool Load(SDL_Renderer* renderer) = 0;
    virtual void Update(const double dt) = 0;
    virtual void Draw(SDL_Renderer* renderer, const double interpolation) const = 0;

    bool IsRunning() const { return !mQuit; }
    void AddEvent(SDL_Event e) { eventQueue.push(e); }

protected:
    bool mQuit;
    bool mLoaded;

    std::queue<SDL_Event> eventQueue;

    virtual void Free() = 0;
};
#endif