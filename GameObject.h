#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "Globals.h"

struct Anchor { float x; float y; };

class GameObject {
public:
    // Constructors
    GameObject() 
        : mX(0), mY(0), mW(0), mH(0), mAnchor(NULL) {
        mAnchor = new Anchor({ 0, 0 });
    }
    GameObject(const float x, const float y) 
        : mX(x), mY(y), mW(0), mH(0), mAnchor(NULL) {
        mAnchor = new Anchor({ 0, 0 });
    }
    GameObject(const float x, const float y, const float w, const float h) 
        : mX(x), mY(y), mW(w), mH(h), mAnchor(NULL) {
        mAnchor = new Anchor({ 0, 0 });
    }
    virtual ~GameObject() { delete mAnchor; mAnchor = NULL; }

    float GetX() const { return mX; }
    float GetY() const { return mY; }
    float GetWidth() const { return mW; }
    float GetHeight() const { return mH; }
    void SetX(float value) { mX = value; }
    void SetY(float value) { mY = value; }


    void Move(float x, float y) {
        mX += x;
        mY += y;
    }

    void SetPosition(float x, float y) {
        mX = x;
        mY = y;
    }

    SDL_Rect GetBoundingBox() const {
        SDL_Rect rect = { mX, mY, mW, mH };
        return rect;
    }

    float GetCenterX() const { return mX + mW / 2; }
    float GetCenterY() const { return mY + mH / 2; }

    void SetAnchorRelative(const float x, const float y) {
        mAnchorOffsetX = x;
        mAnchorOffsetY = y;
        UpdateAnchor();
    }

    Anchor* GetAnchor() const { return mAnchor; }

    void Update(const double dt) {
        UpdateAnchor();
    }

protected:
    void UpdateAnchor() {
        mAnchor->x = mX + mAnchorOffsetX;
        mAnchor->y = mY + mAnchorOffsetY;
    }

    Anchor* mAnchor;
    float mAnchorOffsetX, mAnchorOffsetY;
    float mX, mY, mW, mH;
};

#endif