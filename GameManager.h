#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <SDL.h>
#include "PongGameWorld.h"
#include "Timer.h"

enum GameState { RUNNING = 0, PAUSED = 1, MAINMENU = 2, GAMEMENU = 3 };

class GameManager {
public:
    GameManager(SDL_Renderer*);
    ~GameManager();

    bool Init();
    void Run();
    void Free();

private:
    SDL_Renderer* mRenderer;
    GameWorld* mGameWorld;
    GameState mState;
    Timer mTimer;

    const int TICKS_PER_SECOND = 60;
    const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
    const int MAX_FRAMESKIP = 5;
    const double DIV = 1000.0;
};
#endif