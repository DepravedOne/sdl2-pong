#include "EnemyMovement.h"

void EnemyMovement::Update(const double dt, const Ball* const ball = NULL) {
    // Change random offset after a while
    if (SDL_GetTicks() > nextRandTick) {
        offset = rand.NextDoubleGaussian(0.0, 10.0);
        nextRandTick = SDL_GetTicks() + 500;
    }

    // Move bat
    static const int lerpspeed = 5;
    if (ball != NULL) { // && ball->GetExternalAnchor() != mParent->GetAnchor())  {
        mParent->SetY(Lerp(mParent->GetY(), ball->GetY() - (mParent->GetHeight() / 2 - ball->GetHeight() / 2) + offset, lerpspeed*dt));
    }

    // Restrict movement to screen
    if (mParent->GetY() + mParent->GetHeight() > SCREEN_HEIGHT) {
        mParent->SetY(SCREEN_HEIGHT - mParent->GetHeight());
    }
    else if (mParent->GetY() < 0) {
        mParent->SetY(0);
    }
}