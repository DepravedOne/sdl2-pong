#ifndef COLLISION_H
#define COLLISION_H

#include <SDL.h>

static bool Collides(SDL_Rect* a, SDL_Rect* b) {
    float leftA, leftB, rightA, rightB, topA, topB, bottomA, bottomB;

    leftA = a->x;
    rightA = a->x + a->w;
    topA = a->y;
    bottomA = a->y + a->h;

    leftB = b->x;
    rightB = b->x + b->w;
    topB = b->y;
    bottomB = b->y + b->h;

    return !(bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB);
}

#endif
