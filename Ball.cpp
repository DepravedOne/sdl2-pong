#include "Ball.h"

void Ball::Update(const double dt) {
    // If attached to a paddle, stick to it
    if (mExternalAnchor != NULL) {
        mX = mExternalAnchor->x;
        mY = mExternalAnchor->y;
    }
    // Otherwise update motion 
    else {
        mX += mVelX * dt;
        mY += mVelY * dt;
    }

    mRect = { mX, mY, mW, mH };
}

void Ball::Bounce(float angle) {
    ReleaseAnchor();

    // length of total velocity vector
    float v = mSpeed;
    // x component
    mVelX = cosf(angle) * v;
    // y component
    mVelY = sinf(angle) * v;

    mSpeed *= 1.05;
}

void Ball::Draw(SDL_Renderer* renderer, const double interpolation) const {
    SDL_SetRenderDrawColor(renderer, mColor.r, mColor.g, mColor.b, mColor.a);
    SDL_RenderFillRect(renderer, &mRect);
}

void Ball::Reset() {
    mSpeed = 350.0f;
    mVelX = 0;
    mVelY = 0;
    mW = 10;
    mH = 10;
    mX = 0;
    mY = 0;
}