#ifndef GLOBALS_H
#define GLOBALS_H

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

template <typename T> static int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

static float Lerp(float v0, float v1, float t) {
    return v0 + (v1 - v0) * t;
}
#endif
