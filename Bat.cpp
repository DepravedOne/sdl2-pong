#include "Bat.h"

void Bat::Update(const double dt, const Ball* const ball) {
    // Handle movement
    if (mMoveComp != NULL)
        mMoveComp->Update(dt, ball);

    // Update "image"
    mRect = { mX, mY, mW, mH };

    GameObject::Update(dt);
}

void Bat::Draw(SDL_Renderer* renderer, const float interpolation) const {
    SDL_SetRenderDrawColor(renderer, mColor.r, mColor.g, mColor.b, mColor.a);
    SDL_RenderFillRect(renderer, &mRect);
}

void Bat::Free() {
    delete mMoveComp;
    mMoveComp = NULL;
}