#include "Timer.h"

Timer::Timer() {
    mStartTicks = 0;
    mPausedTicks = 0;
    mPaused = 0;
    mStarted = 0;
}

void Timer::Start() {
    mStarted = true;
    mPaused = false;

    mStartTicks = SDL_GetTicks();
    mPausedTicks = 0;
}

void Timer::Restart() {
    Start();
}

void Timer::Stop() {
    mStarted = false;
    mPaused = false;

    mStartTicks = 0;
    mPausedTicks = 0;
}

void Timer::Pause() {
    if (mStarted && !mPaused) {
        mPaused = true;
        mPausedTicks = SDL_GetTicks() - mStartTicks;
        mStartTicks = 0;
    }
}

void Timer::Resume() {
    if (mStarted && mPaused) {
        mPaused = false;
        mStartTicks = SDL_GetTicks() - mPausedTicks;
        mPausedTicks = 0;
    }
}

Uint32 Timer::GetTicks() {
    Uint32 time = 0;

    if (mStarted) {
        if (mPaused)
            time = mPausedTicks;
        else
            time = SDL_GetTicks() - mStartTicks;
    }
    return time;
}

double Timer::ElapsedSeconds() {
    return GetTicks() / 1000.0;
}