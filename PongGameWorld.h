#ifndef PONGGAMEWORLD_H
#define PONGGAMEWORLD_H

#include <SDL.h>
#include <cmath>
#include "GameWorld.h"
#include "Collision.h"
#include "Bat.h"
#include "Ball.h"
#include "Random.h"

class PongGameWorld : public GameWorld {
public:
    PongGameWorld() : GameWorld(), mPlayer(NULL), mEnemy(NULL), mBall(NULL) { }
    ~PongGameWorld();

    bool Load(SDL_Renderer* renderer) override;
    void Update(const double dt) override;
    void Draw(SDL_Renderer* renderer, const double interpolation) const override;

private:
    Bat* mPlayer;
    Bat* mEnemy;
    Ball* mBall;
    Random mRand;

    void Free() override;
    void CheckBallInBounds();
    void CheckCollision();
    void HandleInput();
    void HandleEvent(SDL_Event*);
};
#endif