#ifndef TIMER_H
#define TIMER_H

#include <SDL.h>

class Timer {
public:
    Timer();

    void Start();
    void Restart();
    void Stop();
    void Pause();
    void Resume();

    inline Uint32 GetTicks();
    double ElapsedSeconds();

    bool IsStarted() const { return mStarted; }
    bool IsPaused() const { return mPaused; }

private:
    Uint32 mStartTicks;
    Uint32 mPausedTicks;
    
    bool mPaused;
    bool mStarted;
};

#endif