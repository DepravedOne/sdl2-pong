#ifndef RANDOM_H
#define RANDOM_H

#include <random>

class Random {
public:
    double NextDoubleUniform(const double min, const double max) {
        std::uniform_real_distribution<double> distribution(min, max);
        return distribution(generator);
    }

    double NextDoubleGaussian(const double mean, const double stddev) {
        std::normal_distribution<double> distribution(mean, stddev);
        return distribution(generator);
    }

private:
    std::default_random_engine generator;
};
#endif