#include "PongGameWorld.h"

PongGameWorld::~PongGameWorld() {
    Free();
}

bool PongGameWorld::Load(SDL_Renderer* renderer) {
    mBall = new Ball();
    mPlayer = new Bat(10, SCREEN_HEIGHT/2 -40, 10, 80, true);
    mEnemy = new Bat(SCREEN_WIDTH - 20, SCREEN_HEIGHT / 2 - 40, 10, 80, false);

    mPlayer->SetAnchorRelative(mPlayer->GetWidth(), mPlayer->GetHeight() / 2 - mBall->GetHeight() / 2);
    mEnemy->SetAnchorRelative(-mBall->GetWidth(), mEnemy->GetHeight() / 2 - mBall->GetHeight() / 2);

    // Place ball at player bat
    mBall->SetAnchor(mPlayer->GetAnchor());
    return true;
}

void PongGameWorld::Update(const double dt) {
    // Handle events on queue
    SDL_Event e;
    while (!eventQueue.empty()) {
        e = eventQueue.front();
        eventQueue.pop();

        HandleEvent(&e);
    }

    if ((mBall->GetExternalAnchor() == mEnemy->GetAnchor() && mRand.NextDoubleUniform(0.0, 1.0) < 0.01)) {
        mBall->Bounce(mBall->GetX() < SCREEN_WIDTH / 2 ? 0.0f : (float)M_PI);
    }

    mPlayer->Update(dt);
    mEnemy->Update(dt, mBall);
    mBall->Update(dt);

    CheckBallInBounds();
    CheckCollision();
}

void PongGameWorld::CheckBallInBounds() {
    // Player scores
    if (mBall->GetX() > SCREEN_WIDTH) {
        if (mBall->GetCenterY() <= mEnemy->GetY() || mBall->GetCenterY() >= mEnemy->GetY() + mEnemy->GetHeight()) {
            mBall->Reset();
            mBall->SetAnchor(mEnemy->GetAnchor());
            mPlayer->IncrementScore();
        }
        else {
            mBall->FlipXVelocity();
        }
    }
    // Enemy scores
    else if (mBall->GetX() + mBall->GetWidth() < 0) {
        if (mBall->GetCenterY() <= mPlayer->GetY() || mBall->GetCenterY() >= mPlayer->GetY() + mPlayer->GetHeight()) {
            mBall->Reset();
            mBall->SetAnchor(mPlayer->GetAnchor());
            mEnemy->IncrementScore();
        }
        else {
            mBall->FlipXVelocity();
        }
    }

    // Hit top or bottom of screen
    if (mBall->GetY() + mBall->GetHeight() > SCREEN_HEIGHT) {
        mBall->SetY(SCREEN_HEIGHT - mBall->GetHeight());
        mBall->FlipYVelocity();
    }
    else if (mBall->GetY() < 0) {
        mBall->SetY(0);
        mBall->FlipYVelocity();
    }
}

void PongGameWorld::CheckCollision() {
    SDL_Rect player = mPlayer->GetBoundingBox();
    SDL_Rect enemy = mEnemy->GetBoundingBox();
    SDL_Rect ball = mBall->GetBoundingBox();

    if (Collides(&player, &ball)) {
        mBall->SetX(mPlayer->GetX() + mPlayer->GetWidth());
        float dy = (mBall->GetCenterY() - mPlayer->GetCenterY()) / (mPlayer->GetHeight() / 2);
        float angle = atanf(dy);
        mBall->Bounce(angle);

    }
    if (Collides(&enemy, &ball)) {
        mBall->SetX(mEnemy->GetX() - mBall->GetWidth());
        float dy = (mBall->GetCenterY() - mEnemy->GetCenterY()) / (mEnemy->GetHeight() / 2);
        float angle = M_PI - atanf(dy);
        mBall->Bounce(angle);
    }
}

void PongGameWorld::HandleEvent(SDL_Event* e) {
    if (e->type == SDL_KEYDOWN) {
        if (e->key.keysym.sym == SDLK_SPACE) {
            if (mBall->GetExternalAnchor() == mPlayer->GetAnchor())
                mBall->Bounce(mBall->GetX() < SCREEN_WIDTH / 2 ? 0.0f : (float)M_PI);
        }
    }
}

void PongGameWorld::Draw(SDL_Renderer* renderer, const double interpolation) const {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    mEnemy->Draw(renderer, interpolation);
    mPlayer->Draw(renderer, interpolation);
    mBall->Draw(renderer, interpolation);

    SDL_RenderPresent(renderer);
}

void PongGameWorld::Free() {
    delete mPlayer;
    mPlayer = NULL;
    delete mEnemy;
    mEnemy = NULL;

    delete mBall;
    mBall = NULL;
}