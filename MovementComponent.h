#ifndef MOVEMENTCOMPONENT_H
#define MOVEMENTCOMPONENT_H

#include <SDL.h>
#include "GameObject.h"
#include "Ball.h"

class MovementComponent {
public:
    MovementComponent(GameObject* parent) : mParent(parent) { }
    virtual ~MovementComponent() { mParent = NULL; }
    virtual void Update(const double dt, const Ball* const ball) = 0;

protected:
    const float speed = 500.0f;
    GameObject* mParent;
};

#endif