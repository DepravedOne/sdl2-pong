#ifndef BAT_H
#define BAT_H

#include <SDL.h>
#include "EnemyMovement.h"
#include "GameObject.h"
#include "PlayerMovement.h"
#include "Ball.h"

class Bat : public GameObject {
public:
    // Constructor
    Bat(const float x, const float y, const float w, const float h, const bool player) : GameObject(x, y, w, h),
        mRect({ mX, mY, mW, mH }), mColor({ 255, 255, 255, 255 }) {

        if (player)
            mMoveComp = new PlayerMovement(this);
        else
            mMoveComp = new EnemyMovement(this);

        mScore = 0;
    }
    // Destructor
    ~Bat() { Free(); }

    void Update(const double dt, const Ball* const ball = NULL);
    void Draw(SDL_Renderer* renderer, const float interpolation) const;

    void IncrementScore() { ++mScore; }

private:
    MovementComponent* mMoveComp;
    const SDL_Color mColor;
    SDL_Rect mRect;
    int mScore;

    void Free();
};

#endif