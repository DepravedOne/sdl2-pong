#include "GameManager.h"

GameManager::GameManager(SDL_Renderer* renderer) : mRenderer(NULL), mGameWorld(NULL) {
    mRenderer = renderer;
    mTimer = Timer();
}

GameManager::~GameManager() {
    Free();
}

bool GameManager::Init() {
    mGameWorld = new PongGameWorld();
    if (mGameWorld == NULL || mRenderer == NULL || !mGameWorld->Load(mRenderer)) {
        Free();
        return false;
    }
    mState = RUNNING;
    mTimer.Stop();

    return true;
}

// Main game manager loop
void GameManager::Run() {
    Uint32 nextGameTick = SDL_GetTicks();
    double prevTime = nextGameTick / DIV;
    double currTime;
    int loops;
    float interpolation;

    SDL_Event e;

    mTimer.Restart();

    bool quit = false;
    while (!quit) {
        // Handle input
        while (SDL_PollEvent(&e) != 0) {
            // User requests quit
            if (e.type == SDL_QUIT) {
                quit = true;
            }
            else {
                if (e.type == SDL_KEYDOWN) {
                    // Quit game with escape (for now)
                    if (e.key.keysym.sym == SDLK_ESCAPE)
                        quit = true;
                    // Pause/unpause game
                    if (e.key.keysym.sym == SDLK_PAUSE) {
                        if (mState == RUNNING) {
                            mTimer.Pause();
                            mState = PAUSED;
                        }
                        else if (mState == PAUSED) {
                            mTimer.Resume();
                            mState = RUNNING;
                        }
                    }
                }

                // If the game running, add the event to its queue
                if (mState == RUNNING) {
                    // Add event to game's event queue
                    mGameWorld->AddEvent(SDL_Event(e));
                }
            }
        }

        // Execute game loop 
        if (mState == RUNNING) {
            loops = 0;
            while (SDL_GetTicks() > nextGameTick && loops < MAX_FRAMESKIP) {
                mGameWorld->Update(mTimer.ElapsedSeconds());
                mTimer.Restart();

                nextGameTick += SKIP_TICKS;
                ++loops;
            }

            interpolation = float(SDL_GetTicks() + SKIP_TICKS - nextGameTick) / float(SKIP_TICKS);
            mGameWorld->Draw(mRenderer, interpolation);
        }
        // Here goes code for handling other stuff, like main menu
    }

}

void GameManager::Free() {
    delete mGameWorld;
    mGameWorld = NULL;
}